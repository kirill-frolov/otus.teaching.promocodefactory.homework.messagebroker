﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;
using Otus.Teaching.Pcf.Messages;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers
{
    public class PromoCodeMapper
    {
        public static PromoCode MapFromModel(GivePromoCodeRequest request, Preference preference, IEnumerable<Customer> customers) {

            var promocode = new PromoCode();
            promocode.Id = request.PromoCodeId;
            
            promocode.PartnerId = request.PartnerId;
            promocode.Code = request.PromoCode;
            promocode.ServiceInfo = request.ServiceInfo;
           
            promocode.BeginDate = DateTime.Parse(request.BeginDate);
            promocode.EndDate = DateTime.Parse(request.EndDate);

            promocode.Preference = preference;
            promocode.PreferenceId = preference.Id;

            promocode.Customers = new List<PromoCodeCustomer>();

            foreach (var item in customers)
            {
                promocode.Customers.Add(new PromoCodeCustomer()
                {

                    CustomerId = item.Id,
                    Customer = item,
                    PromoCodeId = promocode.Id,
                    PromoCode = promocode
                });
            };

            return promocode;
        }

        public static PromoCodeDto MapDto(GivePromoCodeRequest request)
        {
            return new PromoCodeDto
            {
                PreferenceId = request.PreferenceId,
                BeginDate = request.BeginDate,
                EndDate = request.EndDate,
                PartnerId = request.PartnerId,
                PromoCodeId = request.PromoCodeId,
                ServiceInfo = request.ServiceInfo,
                PromoCode = request.PromoCode,
            };
        }

        public static PromoCodeDto MapDto(PromoCodeMessage message)
        {
            return new PromoCodeDto
            {
                PreferenceId = message.PreferenceId,
                BeginDate = message.BeginDate,
                EndDate = message.EndDate,
                PartnerId = message.PartnerId,
                PromoCodeId = message.PromoCodeId,
                ServiceInfo = message.ServiceInfo,
                PromoCode = message.PromoCode,
            };
        }
    }
}
