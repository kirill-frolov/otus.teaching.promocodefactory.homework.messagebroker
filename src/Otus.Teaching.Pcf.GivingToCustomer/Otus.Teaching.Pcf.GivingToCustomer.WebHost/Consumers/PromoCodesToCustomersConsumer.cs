﻿using MassTransit;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Services;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers;
using Otus.Teaching.Pcf.Messages;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Consumers
{
    public class PromoCodesToCustomersConsumer : IConsumer<PromoCodeMessage>
    {
        private IPromoCodesToCustomersService _promoCodesToCustomersService;
        private ILogger<PromoCodesToCustomersConsumer> _logger;

        public PromoCodesToCustomersConsumer(
            ILogger<PromoCodesToCustomersConsumer> logger,
            IPromoCodesToCustomersService promoCodesToCustomersService)
        {
            _promoCodesToCustomersService = promoCodesToCustomersService;
            _logger = logger;
        }
        public async Task Consume(ConsumeContext<PromoCodeMessage> context)
        {
            try
            {
                await _promoCodesToCustomersService.GivePromoCodesToCustomersWithPreferenceAsync(PromoCodeMapper.MapDto(context.Message));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }
    }
}
