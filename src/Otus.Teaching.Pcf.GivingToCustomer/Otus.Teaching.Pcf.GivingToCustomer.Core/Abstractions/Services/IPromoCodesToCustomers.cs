﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Services
{
    public interface IPromoCodesToCustomersService
    {
        Task GivePromoCodesToCustomersWithPreferenceAsync(PromoCodeDto dto);
    }
}