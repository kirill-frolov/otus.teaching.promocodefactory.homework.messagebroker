﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Services;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Services
{
    public class PromoCodesToCustomersService : IPromoCodesToCustomersService
    {
        private IRepository<PromoCode> _promoCodesRepository;
        private IRepository<Preference> _preferencesRepository;
        private IRepository<Customer> _customersRepository;

        public PromoCodesToCustomersService(
            IRepository<PromoCode> promoCodesRepository,
            IRepository<Preference> preferencesRepository,
            IRepository<Customer> customersRepository)
        {
            _promoCodesRepository = promoCodesRepository;
            _preferencesRepository = preferencesRepository;
            _customersRepository = customersRepository;
        }
        public async Task GivePromoCodesToCustomersWithPreferenceAsync(PromoCodeDto dto)
        {
            //Получаем предпочтение по имени
            var preference = await _preferencesRepository.GetByIdAsync(dto.PreferenceId);

            if (preference == null)
                throw new EntityNotFoundException();

            //  Получаем клиентов с этим предпочтением:
            var customers = await _customersRepository
                .GetWhere(d => d.Preferences.Any(x =>
                    x.Preference.Id == preference.Id));

            PromoCode promoCode = MapFromModel(dto, preference, customers);
            await _promoCodesRepository.AddAsync(promoCode);


        }
        private PromoCode MapFromModel(PromoCodeDto dto, Preference preference, IEnumerable<Customer> customers)
        {

            var promocode = new PromoCode();
            promocode.Id = dto.PromoCodeId;

            promocode.PartnerId = dto.PartnerId;
            promocode.Code = dto.PromoCode;
            promocode.ServiceInfo = dto.ServiceInfo;

            promocode.BeginDate = DateTime.Parse(dto.BeginDate);
            promocode.EndDate = DateTime.Parse(dto.EndDate);

            promocode.Preference = preference;
            promocode.PreferenceId = preference.Id;

            promocode.Customers = new List<PromoCodeCustomer>();

            foreach (var item in customers)
            {
                promocode.Customers.Add(new PromoCodeCustomer()
                {

                    CustomerId = item.Id,
                    Customer = item,
                    PromoCodeId = promocode.Id,
                    PromoCode = promocode
                });
            };

            return promocode;
        }

    }
}
