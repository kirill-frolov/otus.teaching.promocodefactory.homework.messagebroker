﻿using System;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Exceptions
{
    public class EntityNotFoundException : Exception
    {
        public EntityNotFoundException() : base("Entity not found") { }
    }
}
