﻿using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.Core.Abstractions.Services
{
    public interface IPartnerManagerPromoCodeService
    {
        Task UpdateAppliedPromocodesAsync(Guid id);
    }
}