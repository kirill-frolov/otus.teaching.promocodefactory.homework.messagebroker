﻿using System;

namespace Otus.Teaching.Pcf.Administration.Core.Exceptions
{
    public class EntityNotFoundException : Exception
    {
        public EntityNotFoundException() : base("Entity not found") { }
    }
}
