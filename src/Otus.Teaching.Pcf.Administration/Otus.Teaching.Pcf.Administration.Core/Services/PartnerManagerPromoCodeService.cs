﻿using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Services;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.Core.Exceptions;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.Core.Services
{
    public class PartnerManagerPromoCodeService : IPartnerManagerPromoCodeService
    {
        private IRepository<Employee> _employeeRepository;

        public PartnerManagerPromoCodeService(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        public async Task UpdateAppliedPromocodesAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);
            if (employee == null)
               throw new EntityNotFoundException();
            employee.AppliedPromocodesCount++;
            await _employeeRepository.UpdateAsync(employee);



        }
    }
}
