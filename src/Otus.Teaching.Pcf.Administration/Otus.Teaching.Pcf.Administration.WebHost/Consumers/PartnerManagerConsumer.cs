﻿using System;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Services;
using Otus.Teaching.Pcf.Messages;

namespace Otus.Teaching.Pcf.Administration.WebHost.Consumers
{
    public class PartnerManagerConsumer : IConsumer<PartnerManagerPromoCodeMessage>
    {
        private ILogger<PartnerManagerConsumer> _logger;
        private IPartnerManagerPromoCodeService _partnerManagerPromoCodeService;

        public PartnerManagerConsumer(
            ILogger<PartnerManagerConsumer> logger,
            IPartnerManagerPromoCodeService partnerManagerPromoCodeService)
        {
            _logger = logger;
            _partnerManagerPromoCodeService = partnerManagerPromoCodeService;
        }
        public async Task Consume(ConsumeContext<PartnerManagerPromoCodeMessage> context)
        {
            try
            {
                await _partnerManagerPromoCodeService.UpdateAppliedPromocodesAsync(context.Message.PartnerManagerId);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

        }
    }
}
