﻿using System;

namespace Otus.Teaching.Pcf.Messages
{
    public class PartnerManagerPromoCodeMessage
    {
        public Guid PartnerManagerId { get; set; }

    }
}
